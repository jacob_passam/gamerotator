package gamerotator.cmd;

import gamerotator.perm.Permission;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

public abstract class CommandBase {

    private final boolean CONSOLE_DISABLED_DEFAULT = false;

    @Getter private final String name;
    @Getter @Setter private List<String> aliases = new ArrayList<>();
    @Getter @Setter private Permission perm;
    @Getter @Setter private boolean consoleDisabled = CONSOLE_DISABLED_DEFAULT;

    public CommandBase(String name) {
        this.name = name;
    }

    public abstract void execute(CommandSender sender, String[] args);
    public abstract List<String> tabComplete(CommandSender sender, String[] args);


}
