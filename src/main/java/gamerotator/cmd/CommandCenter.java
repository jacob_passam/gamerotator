package gamerotator.cmd;

import gamerotator.message.Messages;
import gamerotator.perm.Permission;
import gamerotator.perm.PermissionManager;
import gamerotator.util.UtilServer;
import lombok.SneakyThrows;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.util.List;

public class CommandCenter {

    @SneakyThrows
    public static void register(final CommandBase commandBase, final PermissionManager permissionManager) {

        final Command command = new Command(commandBase.getName()) {
            @Override
            public boolean execute(CommandSender commandSender, String s, String[] strings) {

                if (commandBase.isConsoleDisabled() && !(commandSender instanceof Player)) {
                    commandSender.sendMessage(Messages.CONSOLE_DISABLED);
                    return true;
                }

                if (commandSender instanceof Player) {
                    Player player = (Player) commandSender;
                    Permission permission = commandBase.getPerm();

                    if (!(permissionManager.hasPermission(player.getUniqueId(), permission))) {
                        player.sendMessage(Messages.NO_PERMISSION);
                        return true;
                    }
                }

                commandBase.execute(commandSender, strings);

                return false;
            }

            @Override
            public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {

                if (commandBase.isConsoleDisabled() && !(sender instanceof Player)) return null;

                if (sender instanceof Player) {
                    Player player = (Player) sender;
                    if (!(permissionManager.hasPermission(player.getUniqueId(), commandBase.getPerm()))) {
                        return null;
                    }
                }

                return commandBase.tabComplete(sender, args);

            }
        };

        command.setAliases(commandBase.getAliases());

        Field bukkitCommandMap = UtilServer.getServer().getClass().getDeclaredField("commandMap");
        bukkitCommandMap.setAccessible(true);

        CommandMap map = (CommandMap) bukkitCommandMap.get(UtilServer.getServer());
        map.register("gamerotator", command);

    }
}
