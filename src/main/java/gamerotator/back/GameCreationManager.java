package gamerotator.back;

import gamerotator.GameManager;
import gamerotator.event.GameSetEvent;
import gamerotator.game.GameType;
import lombok.SneakyThrows;
import org.bukkit.Bukkit;

public class GameCreationManager {

    private final GameManager gameManager;

    public GameCreationManager(GameManager gameManager) {
        this.gameManager = gameManager;
    }

    @SneakyThrows
    public void setGame(GameType gameType, GameSetEvent.SetReason reason) {
        GameManager.CURRENT_GAME = gameType.getGame().getConstructor(GameManager.class).newInstance(gameManager);

        Bukkit.getServer().getPluginManager().callEvent(
                new GameSetEvent(GameManager.CURRENT_GAME, gameManager, reason)
        );

        gameManager.log("GameCreationManager: Game has been set to " +gameType.name() + " due to " + reason);
    }
}
