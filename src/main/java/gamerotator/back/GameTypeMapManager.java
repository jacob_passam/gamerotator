package gamerotator.back;

import com.gitlab.jacobpassam.mapparser.ParsedMap;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import gamerotator.game.GameType;
import lombok.SneakyThrows;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GameTypeMapManager {

    private final JavaPlugin host;
    private final Map<GameType, ArrayList<ParsedMap>> mapCache;

    public GameTypeMapManager(JavaPlugin host) {
        this.host = host;
        this.mapCache = new HashMap<>();
    }

    @SneakyThrows
    public JsonObject getMapsFileContent() {
        InputStream inputStream = host.getResource("map-data.json");

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

        String str = "", line = "";
        while ((line = bufferedReader.readLine()) != null) {
            str += line;
        }

        JsonObject object = new JsonParser().parse(str).getAsJsonObject();

        return object;
    }

    public String[] getMapNamesForGameType(GameType type) {
        JsonObject content = getMapsFileContent();

        JsonArray mapNameArray = content.get(type.name()).getAsJsonArray();

        List<String> wrappedList = new ArrayList<>();
        for (JsonElement jsonElement : mapNameArray) {
            wrappedList.add(jsonElement.getAsString());
        }

        return wrappedList.toArray(new String[]{});
    }

    @SneakyThrows
    public ParsedMap getMapData(GameType type, String mapName) {
        if (mapCache.containsKey(type)) {

            ArrayList<ParsedMap> maps = mapCache.get(type);

            for (ParsedMap map : maps) {
                if (map.getMapName().equals(mapName)) {
                    return map;
                }
            }
        } else {
            mapCache.put(type, new ArrayList<>());
        }

        String str = "", line = "";
        InputStream inputStream = host.getResource(ParsedMap.mapString(type.name(), mapName));
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        while ((line = reader.readLine()) != null) {
            str += line;
        }

        ParsedMap map = ParsedMap.fromJson(str);

        mapCache.get(type).add(map);
        return map;
    }

}
