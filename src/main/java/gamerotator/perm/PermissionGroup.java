package gamerotator.perm;

import lombok.Getter;
import org.bukkit.ChatColor;

import java.util.HashMap;
import java.util.List;

public enum PermissionGroup {

    DEFAULT("Default", "The default permission group.",  ChatColor.GRAY, true),
    DEFAULT_ENHANCED("Default-Enhanced", "Default, but with extra spice.",  ChatColor.GRAY, true, DEFAULT),
    MODERATION("Moderation", "The group containing moderation permissions.", ChatColor.YELLOW, true, DEFAULT_ENHANCED),
    ADMINISTRATOR("Administrator", "The group containing administration permissions.", ChatColor.RED, true, MODERATION),

    EVENT_MODERATION("Event Moderation", "The event moderation group.", ChatColor.GREEN, false, MODERATION),
    EVENT_LEAD("Event Lead", "The event lead group", ChatColor.GREEN, false, EVENT_MODERATION);

    @Getter private final String name, description;
    @Getter private final ChatColor color;
    @Getter private final boolean isPrimary;
    @Getter private final PermissionGroup[] parentGroups;

    PermissionGroup(String name, String description, ChatColor color, boolean isPrimary, PermissionGroup... parentGroups) {
        this.name = name;
        this.color = color;
        this.description = description;
        this.isPrimary = isPrimary;
        this.parentGroups = parentGroups;
    }




}
