package gamerotator.perm;

/**
 * A permission that can be paired with {@link PermissionGroup}.
 * <p>
 * Designed only to be used paired with enums, as only enum-based permissions
 * implementing this interface are accepted by PermissionGroup.
 * </p>
 *
 * Example usage:
 * <pre>
 * {@code
 * public enum ModulePerm implements Permission { PERMISSION_ONE, PERMISSION_TWO }
 * public void generatePermissions() { permissionManager.addPermission(PermissionGroup.ADMINISTRATOR, ModulePerm.PERMISSION_ONE); }
 * </pre>
 */
public interface Permission {
}
