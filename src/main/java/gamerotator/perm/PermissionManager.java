package gamerotator.perm;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.mongodb.client.MongoCollection;
import gamerotator.GameRotator;
import gamerotator.db.MongoHandler;
import org.bson.Document;

import java.util.*;

public class PermissionManager {

    private final MongoHandler mongoHandler;
    private final HashMap<PermissionGroup, Set<Permission>> permissionMap;

    public PermissionManager(MongoHandler mongoHandler) {
        this.mongoHandler = mongoHandler;

        this.permissionMap = new HashMap<>();
        for (PermissionGroup value : PermissionGroup.values()) {
            permissionMap.put(value, new HashSet<Permission>());
        }
    }

    public void addPermission(PermissionGroup group, Permission permission) {
        this.permissionMap.get(group).add(permission);
    }

    public void revokePermission(PermissionGroup group, Permission permission) {
        this.permissionMap.get(group).remove(permission);
    }

    public boolean hasPermission(UUID uuid, Permission permission) {
        List<PermissionGroup> directPermGroups = getPermissionGroupsFromUuid(uuid);
        Set<PermissionGroup> presentPermGroups = getGroupsWherePermissionIsDirectlyPresent(permission);

        for (PermissionGroup permissionGroup : directPermGroups) {
            // Got it directly
            if (presentPermGroups.contains(permissionGroup)) return true;

            for (PermissionGroup parentGroup : permissionGroup.getParentGroups()) {
                // Got it through inheritance
                if (presentPermGroups.contains(parentGroup)) return true;
            }
        }

        // AUTHOR ALWAYS HAS ALL PERMISSIONS
        return uuid.equals(GameRotator.AUTHOR_UUID);
    }

    private Set<PermissionGroup> getGroupsWherePermissionIsDirectlyPresent(Permission permission) {
        Set<PermissionGroup> temp = new HashSet<>();
        for (PermissionGroup permissionGroup : permissionMap.keySet()) {
            if (permissionMap.get(permissionGroup).contains(permission)) {
                temp.add(permissionGroup);
            }
        }

        return temp;
    }

    private List<PermissionGroup> getPermissionGroupsFromUuid(UUID uuid) {
        MongoCollection<Document> collection = mongoHandler.getPermissions();

        Document doc = collection.find(new Document("uuid", uuid.toString())).first();

        if (doc == null) {
            return new ArrayList<>(Collections.singletonList(PermissionGroup.DEFAULT));
        }

        List<String> list = doc.getList("groups", String.class);

        List<PermissionGroup> pgList = new ArrayList<>();
        for (String s : list) pgList.add(PermissionGroup.valueOf(s));
        pgList.add(PermissionGroup.DEFAULT);
        return pgList;
    }

    public void assignGroupToUser(UUID uuid, PermissionGroup permissionGroup) {
        System.out.println("1");
        if (permissionGroup == PermissionGroup.DEFAULT) return;

        List<PermissionGroup> groups = getPermissionGroupsFromUuid(uuid);
        System.out.println("2 " + Arrays.toString(groups.toArray()));
        if (groups.contains(permissionGroup)) return;
        System.out.println("3");
        final Document SEARCH_QUERY = new Document("uuid", uuid.toString());

        MongoCollection<Document> collection = mongoHandler.getPermissions();
        Document doc = collection.find(SEARCH_QUERY).first();

        if (doc == null) {
            System.out.println("4");
            // NOT PRESENT
            JsonObject object = new JsonObject();
            object.addProperty("uuid", uuid.toString());

            JsonArray array = new JsonArray();
            array.add(new JsonPrimitive(permissionGroup.name()));

            object.add("groups", array);

            collection.insertOne(Document.parse(object.toString()));
        } else {
            System.out.println("5");
            // ALREADY PRESENT
            List<String> permGroups = doc.getList("groups", String.class);
            permGroups.add(permissionGroup.name());

            collection.findOneAndUpdate(SEARCH_QUERY, new Document("$set", new Document("groups", permGroups)));
        }
    }


}
