package gamerotator.event;

import gamerotator.GameManager;
import gamerotator.game.Game;
import lombok.Getter;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GameSetEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    public HandlerList getHandlers() { return handlers; }
    public static HandlerList getHandlerList() { return handlers; }

    @Getter private final Game newGame;
    @Getter private final GameManager gameManager;
    @Getter private final SetReason reason;

    public enum SetReason {
        FORCE_SET,
        GAME_CONTINUITY,
        DEFAULT_GAME;
    }

    public GameSetEvent(Game newGame, GameManager gameManager, SetReason reason) {
        this.newGame = newGame;
        this.gameManager = gameManager;
        this.reason = reason;
    }
}
