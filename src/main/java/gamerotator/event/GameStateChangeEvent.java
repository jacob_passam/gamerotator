package gamerotator.event;

import gamerotator.GameManager;
import gamerotator.game.Game;
import gamerotator.game.GameState;
import lombok.Getter;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GameStateChangeEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    public HandlerList getHandlers() { return handlers; }
    public static HandlerList getHandlerList() { return handlers; }

    @Getter private final Game game;
    @Getter private final GameManager gameManager;
    @Getter private GameState oldState;
    @Getter private GameState newState;

    public GameStateChangeEvent(Game game, GameManager gameManager, GameState oldState, GameState newState) {
        this.game = game;
        this.gameManager = gameManager;
        this.oldState = oldState;
        this.newState = newState;
    }
}
