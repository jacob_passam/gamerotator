package gamerotator.message;

import gamerotator.perm.Permission;
import gamerotator.perm.PermissionGroup;
import org.bukkit.ChatColor;

public class Messages {

    public static String CONSOLE_DISABLED = ChatColor.RED + "(!) That command can only be used in-game.";
    public static String NO_PERMISSION = ChatColor.RED + "You do not have permission to execute that command!";

}
