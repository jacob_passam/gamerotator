package gamerotator.iso;

import gamerotator.GameManager;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PrivateServerManager implements Listener {

    private final GameManager gameManager;

    @Getter private final boolean isPrivateServer;
    @Getter @Setter private UUID privateServerHost;

    @Getter @Setter private boolean whitelistEnabled;
    @Getter private final List<UUID> whitelistedUsers;

    public PrivateServerManager(GameManager manager) {
        manager.getHost().getServer().getPluginManager().registerEvents(this, manager.getHost());

        this.gameManager = manager;

        this.isPrivateServer = manager.isPrivateServer();
        this.privateServerHost = UUID.fromString(manager.getHost().getConfig().getString("server_host"));

        this.whitelistEnabled = false;
        this.whitelistedUsers = new ArrayList<>();
    }
}
