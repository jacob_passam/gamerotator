package gamerotator;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class Constants {

    public static final Location WAITING_LOBBY_LOCATION = new Location(Bukkit.getWorld("waiting_lobby"), 23.5, 119.5, 1.5, -90, 0);
    public static final String SERVER_IP = "myserver.com";
}
