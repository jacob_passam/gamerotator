package gamerotator.board;

import com.google.common.collect.LinkedListMultimap;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class ScoreboardBuilder {

    private String displayName;
    private NametagType nametagType;
    private final LinkedListMultimap<String, String> fields;

    /**
     * Users to display nametags to if not using EnemyFriendly option.
     */
    private List<Player> normalTagsPlayers;

    /**
     * Users to have enemy tags.
     */
    private List<Player> enemyTagsPlayer;

    public String getDisplayName() {
        return displayName;
    }

    public ScoreboardBuilder setDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    public NametagType getNametagType() {
        return nametagType;
    }

    public ScoreboardBuilder setNametagType(NametagType nametagType) {
        this.nametagType = nametagType;
        return this;
    }

    public LinkedListMultimap<String, String> getFields() {
        return fields;
    }

    public List<Player> getNormalTagsPlayers() {
        return normalTagsPlayers;
    }

    public List<Player> getEnemyTagsPlayer() {
        return enemyTagsPlayer;
    }

    public List<Player> getFriendlyTagsPlayer() {
        return friendlyTagsPlayer;
    }

    public ScoreboardBuilder setNormalTagsPlayers(List<Player> normalTagsPlayers) {
        this.normalTagsPlayers = normalTagsPlayers;
        return this;
    }

    public ScoreboardBuilder setEnemyTagsPlayer(List<Player> enemyTagsPlayer) {
        this.enemyTagsPlayer = enemyTagsPlayer;
        return this;
    }

    public ScoreboardBuilder setFriendlyTagsPlayer(List<Player> friendlyTagsPlayer) {
        this.friendlyTagsPlayer = friendlyTagsPlayer;
        return this;
    }

    /**
     * Users to have friendly tags.
     */
    private List<Player> friendlyTagsPlayer;

    public ScoreboardBuilder() {
        fields = LinkedListMultimap.create();
        normalTagsPlayers = new ArrayList<>();
        enemyTagsPlayer = new ArrayList<>();
        friendlyTagsPlayer = new ArrayList<>();
    }

    public ScoreboardBuilder addField(String key, String value) {
        if (key.contains("$")) {
            throw new IllegalArgumentException("Fields cannot contain $.");
        }
        fields.put(key, value);
        return this;
    }

    public ScoreboardBuilder addSpacer() {
        fields.put("$SPACER", null);
        return this;
    }

    public ScoreboardBuilder addPreset(BoardPreset preset) {
        fields.put("$" + preset.name(), null);
        return this;
    }

    public Scoreboard build() {
        Scoreboard _board = Bukkit.getServer().getScoreboardManager().getNewScoreboard();

        Objective _bar = _board.registerNewObjective("sidebar", "dummy");
        _bar.setDisplaySlot(DisplaySlot.SIDEBAR);
        _bar.setDisplayName(displayName);

        List<String> spacers = new ArrayList<>(Arrays.asList("§a ", "§b ", "§c ", "§d ", "§e ", "§f ", "§1 ", "§2 ", "§3 ", "§4 ", "§5 ", "§6 ", "§7 ", "§8 ", "§9 "));

        AtomicInteger slot = new AtomicInteger(16);
        fields.entries().forEach(listItem -> {
            String key = listItem.getKey();
            String value = listItem.getValue();

            if (slot.get() == 0) {
                throw new UnsupportedOperationException("Too many fields given.");
            }
            if (key.contains("$")) {
                String option = key.replace("$", "");
                switch (option) {
                    case "SPACER": {
                        String spacer = spacers.get(new Random().nextInt(spacers.size()));
                        spacers.remove(spacer);
                        _bar.getScore(spacer).setScore(slot.get());
                        break;
                    }
                    case "DATE": {
                        String date = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
                        _bar.getScore("§7" + date).setScore(slot.get());
                        break;
                    }
                    case "IP_ADDRESS": {
                        String ip_address = "§bserver.com";
                        _bar.getScore(ip_address).setScore(slot.get());
                        break;
                    }
                }
            } else {
                _bar.getScore("§7" + key + ": §f" + value).setScore(slot.get());
            }

            slot.getAndDecrement();
        });

        switch (nametagType) {
            case NEUTRAL: {
                Team neutralTeam = _board.registerNewTeam("neutralTeam");
                neutralTeam.setPrefix("§7");
                neutralTeam.setNameTagVisibility(NameTagVisibility.ALWAYS);
                normalTagsPlayers.forEach(player -> neutralTeam.addEntry(player.getName()));
                break;
            }
            case WAITING: {
                Team waitingTeam = _board.registerNewTeam("waitingTeam");
                waitingTeam.setPrefix("§e");
                waitingTeam.setNameTagVisibility(NameTagVisibility.ALWAYS);
                normalTagsPlayers.forEach(player -> waitingTeam.addEntry(player.getName()));
                break;
            }
            case ENEMY_FRIENDLY:
                Team enemyTeam = _board.registerNewTeam("enemyTeam");
                Team friendlyTeam = _board.registerNewTeam("friendlyTeam");
                enemyTeam.setPrefix("§c");
                friendlyTeam.setPrefix("§a");

                friendlyTagsPlayer.forEach(player -> friendlyTeam.addEntry(player.getName()));
                enemyTagsPlayer.forEach(player -> enemyTeam.addEntry(player.getName()));
                break;
        }

        return _board;
    }

    public enum BoardPreset {
        DATE, IP_ADDRESS;
    }

    public enum NametagType {
        NEUTRAL, WAITING, ENEMY_FRIENDLY;
    }
}
