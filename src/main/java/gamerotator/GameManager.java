package gamerotator;

import com.gitlab.jacobpassam.mapparser.ParsedMap;
import gamerotator.admin.CommandWorldTeleport;
import gamerotator.back.GameCreationManager;
import gamerotator.back.GameTypeMapManager;
import gamerotator.board.ScoreboardBuilder;
import gamerotator.cmd.CommandCenter;
import gamerotator.event.GameSetEvent;
import gamerotator.event.GameStateChangeEvent;
import gamerotator.game.Game;
import gamerotator.game.GameState;
import gamerotator.game.GameType;
import gamerotator.iso.PrivateServerManager;
import gamerotator.perm.Permission;
import gamerotator.perm.PermissionGroup;
import gamerotator.perm.PermissionManager;
import gamerotator.util.UtilNMS;
import gamerotator.util.UtilWorld;
import lombok.Getter;
import lombok.SneakyThrows;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class GameManager implements Listener {

    public static Game CURRENT_GAME;

    @Getter private final GameRotator host;
    @Getter private final PermissionManager permissionManager;
    private final GameCreationManager gameCreationManager;
    private final GameTypeMapManager gameTypeMapManager;
    private final PrivateServerManager privateServerManager;

    // Map list for current game
    @Getter private List<ParsedMap> gameMaps;

    // Private server configuration
    @Getter private boolean isPrivateServer;

    // Local GameType variable; prevents NPEs when Game is calling methods in here during initialisation
    private GameType gameType;

    @SneakyThrows
    public GameManager(GameRotator host, PermissionManager permissionManager) {
        this.host = host;
        this.permissionManager = permissionManager;

        this.isPrivateServer = host.getConfig().getBoolean("private_server");
        this.privateServerManager = new PrivateServerManager(this);

        log("GameManager: Private server mode is " + isPrivateServer + "");
        log("GameManager: Private server host: " + privateServerManager.getPrivateServerHost().toString());

        this.gameCreationManager = new GameCreationManager(this);

        host.getServer().getPluginManager().registerEvents(this, host);

        this.gameTypeMapManager = new GameTypeMapManager(host);

        this.gameMaps = new ArrayList<>();

        this.gameType = GameType.valueOf(host.getConfig().getString("default_game"));
        gameCreationManager.setGame(gameType, GameSetEvent.SetReason.DEFAULT_GAME);

        log("GameManager: Game object initialised: " + gameType.getGame().getSimpleName());

        generatePermissions();
        log("GameManager: Permissions generated");

        generateCommands();
        log("GameManager: Commands generated");
    }

    private void generateCommands() {
        CommandCenter.register(new CommandWorldTeleport("worldteleport"), permissionManager);
    }

    public enum GamePerm implements Permission { COMMAND_WORLD_TELEPORT }
    private void generatePermissions() {
        permissionManager.addPermission(PermissionGroup.ADMINISTRATOR, GamePerm.COMMAND_WORLD_TELEPORT);
    }

    // Call from game class, I beg...
    public void generateMapsAndAdd() {
        UtilWorld.cleanWorldDirectories(false);

        String[] mapNames = gameTypeMapManager.getMapNamesForGameType(gameType);

        for (String mapName : mapNames) {
            gameMaps.add(gameTypeMapManager.getMapData(gameType, mapName));
        }

        for (ParsedMap gameMap : gameMaps) {
            String format = ParsedMap.mapString(gameType.name(), gameMap.getMapName());
            UtilWorld.downloadWorld(format, host); // Add worlds into root dir; we'll do the loading later.
        }

        log("GameManager: Maps generated, not loaded yet!");
    }

    public void loadMap(String name, boolean removeEntities) {
        UtilWorld.loadWorld(ParsedMap.mapString(gameType.name(), name), removeEntities);
        log("GameManager: Map " + name + " successfully loaded into Bukkit");
    }

    public void reset() {
        CURRENT_GAME = null;
        this.gameMaps.clear();

        log("GameManager: Reset successfully performed.");

        // add more
    }

    public void log(String message) {
        host.getLogger().info(message);
    }

    @EventHandler
    public void gameSet(GameSetEvent event) {
        if (event.getReason() != GameSetEvent.SetReason.DEFAULT_GAME) reset();
    }

    @EventHandler
    public void gameStateChange(GameStateChangeEvent event) {
        if (event.getNewState() == GameState.RUNNING) CURRENT_GAME.setPlayersAtBegin(CURRENT_GAME.getAlivePlayerCount());
    }

    @EventHandler
    public void login(PlayerLoginEvent event) {

        if (isPrivateServer()) {

            if (privateServerManager.isWhitelistEnabled()) {

                if (!(privateServerManager.getWhitelistedUsers().contains(event.getPlayer().getUniqueId()))) {
                    event.disallow(PlayerLoginEvent.Result.KICK_WHITELIST, "§cThat server has whitelist mode enabled, and you are not a whitelisted user.");
                    log("GameManager: Player " + event.getPlayer().getName() + " kicked due to WHITELIST");
                }

            }

        }

        if (CURRENT_GAME.isPreGame()) {
            if (CURRENT_GAME.getAlivePlayerCount() == CURRENT_GAME.getMaximumPlayers()) {
                event.disallow(PlayerLoginEvent.Result.KICK_FULL, "§cThat game has reached maximum capacity for gameplay purposes.");
                log("GameManager: Player " + event.getPlayer().getName() + " kicked due to FULL");
            }
        }
    }

    @EventHandler
    public void join(PlayerJoinEvent event) {
        event.setJoinMessage(null);

        CURRENT_GAME.addPlayer(event.getPlayer());

        if (CURRENT_GAME.isPreGame()) {
            event.getPlayer().teleport(Constants.WAITING_LOBBY_LOCATION);
            sendJoinMessage(event.getPlayer());
            announceJoin(event.getPlayer());

            if (CURRENT_GAME.getGameState() == GameState.WAITING) {
                CURRENT_GAME.getTotalPlayers().forEach((uuid) -> {
                    setWaitingBoard(Bukkit.getPlayer(uuid));
                });
            }

        } else {
            // spectator sort
        }
        // check vanish!!

        sendTabHF(event.getPlayer());
    }

    public void sendJoinMessage(Player player) {
        player.sendMessage(" ");
        player.sendMessage(" ");
        player.sendMessage("§8§m---------------------------------------------");
        player.sendMessage("§eWelcome to §e§l" + gameType.getName());
        player.sendMessage(" ");
        player.sendMessage("§7" + gameType.getDescription());
        player.sendMessage(" ");
        player.sendMessage("§8§m---------------------------------------------");
        player.sendMessage(" ");
    }

    public void announceJoin(Player player) {
        for (UUID totalPlayer : CURRENT_GAME.getTotalPlayers()) {
            Bukkit.getPlayer(totalPlayer).sendMessage("§8Join> §7" + player.getName());
        }
    }

    public void setWaitingBoard(Player player) {
        player.setPlayerListName("§7" + player.getName());

        List<Player> totalPlayers = new ArrayList<>();
        for (UUID totalPlayer : CURRENT_GAME.getTotalPlayers()) {
            totalPlayers.add(Bukkit.getPlayer(totalPlayer));
        }

        player.setScoreboard(
                new ScoreboardBuilder()
                        .setDisplayName("  §b§lMy§f§lServer §7§l- Games  ")

                        .addSpacer()
                        .addField("Game", gameType.getName())
                        .addField("Players", "" + CURRENT_GAME.getAlivePlayerCount())
                        .addSpacer()
                        .addField("Map", "Voting...")
                        .addField("State", "Waiting for Players")
                        .addSpacer()
                        .addPreset(ScoreboardBuilder.BoardPreset.IP_ADDRESS)

                        .setNametagType(ScoreboardBuilder.NametagType.WAITING)
                        .setNormalTagsPlayers(totalPlayers)

                        .build()
        );
    }

    public void sendTabHF(Player player) {
        UtilNMS.sendTabTitle(player,
                "    §7You are playing §b§l" + gameType.getName() + " §7on §b" + Constants.SERVER_IP + "§7.    ",
                "    §7Visit §a§l" + Constants.SERVER_IP + " §7for store, forums, and announcements!"
        );
    }
}
