package gamerotator;

import gamerotator.db.MongoConnector;
import gamerotator.db.MongoHandler;
import gamerotator.perm.PermissionManager;
import gamerotator.util.UtilWorld;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.UUID;

public class GameRotator extends JavaPlugin {

    public static final UUID AUTHOR_UUID = UUID.fromString("84e2de06-9ebe-4dbe-8cb1-462b41042eeb");

    private GameManager gameManager;

    @Override
    public void onEnable() {
        // Pre-clean including waiting lobby on first startup
        UtilWorld.cleanWorldDirectories(true);

        MongoConnector connector = new MongoConnector();
        MongoHandler handler = new MongoHandler(connector);

        PermissionManager permissionManager = new PermissionManager(handler);

        UtilWorld.downloadWorld("waiting_lobby", this);
        UtilWorld.loadWorld("waiting_lobby", true);

        getConfig().addDefault("default_game", "Skywars");

        // Default private server options
        getConfig().addDefault("private_server", false);
        getConfig().addDefault("server_host", "84e2de06-9ebe-4dbe-8cb1-462b41042eeb");
        getConfig().options().copyDefaults(true);
        saveConfig();

        this.gameManager = new GameManager(this, permissionManager);
    }
}
