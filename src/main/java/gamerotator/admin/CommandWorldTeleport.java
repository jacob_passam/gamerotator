package gamerotator.admin;

import gamerotator.GameManager;
import gamerotator.cmd.CommandBase;
import gamerotator.message.Messages;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.WorldCreator;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;

public class CommandWorldTeleport extends CommandBase {

    public CommandWorldTeleport(String name) {
        super(name);
        setConsoleDisabled(true);
        setPerm(GameManager.GamePerm.COMMAND_WORLD_TELEPORT);
        setAliases(Arrays.asList("worldtp", "tpworld"));
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        Player player = (Player) sender;

        if (args.length != 1) {
            player.sendMessage("§c/worldteleport <world>");
            return;
        }

        if (Bukkit.getWorld(args[0]) == null) {
            player.teleport(new Location(new WorldCreator(args[0]).createWorld(), 0, 100, 0));
        } else {
            player.teleport(new Location(Bukkit.getWorld(args[0]), 0, 100, 0));
        }
        player.setGameMode(GameMode.CREATIVE);
        player.setFlying(true);
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String[] args) {
        return null;
    }
}
