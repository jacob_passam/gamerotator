package gamerotator.game.games.skywars;

import gamerotator.GameManager;
import gamerotator.game.GameType;
import gamerotator.game.SoloGame;
import org.bukkit.entity.Player;

public class Skywars extends SoloGame {

    public Skywars(GameManager gameManager) {
        super(gameManager,
                GameType.Skywars,
                2,
                8
                );
    }

    @Override
    public void addPlayerPlus(Player player) {

    }
}
