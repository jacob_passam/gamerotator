package gamerotator.game;

import gamerotator.GameManager;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public abstract class Game {

    @Getter private GameManager gameManager;
    @Getter private final GameType gameType;

    // Maximum amount of players a game can be played with.
    @Getter private int maximumPlayers;

    // Minimum amount of players a game can be played with.
    @Getter private int minimumPlayers;

    // Total players (alive + spectators)
    @Getter private int totalPlayerCount = 0;

    // Alive players (ie. players currently playing the game)
    @Getter private int alivePlayerCount = 0;

    // Spectators (ie. players watching but not playing the game)
    @Getter private int spectatorCount = 0;

    // Players at beginning (gamestate switch to RUNNING)
    // Represents the amount of ALIVE players at the start of the game.
    // DOES NOT INCLUDE SPECTATORS
    @Getter @Setter private int playersAtBegin = 0;

    @Getter @Setter
    private GameState gameState = GameState.WAITING;

    @Getter private List<UUID> alivePlayers;
    @Getter private List<UUID> spectators;

    public List<UUID> getTotalPlayers() {
        ArrayList<UUID> wrapped = new ArrayList<>();
        wrapped.addAll(alivePlayers); wrapped.addAll(spectators);
        return wrapped;
    }

    public Game(
            GameManager gameManager, GameType gameType,
            int minimumPlayers, int maximumPlayers) {
        this.gameManager = gameManager;
        this.gameType = gameType;
        this.minimumPlayers = minimumPlayers;
        this.maximumPlayers = maximumPlayers;
        this.alivePlayers = new ArrayList<>();
        this.spectators = new ArrayList<>();

        // Map generation / setup
        gameManager.generateMapsAndAdd();
    }

    public void addPlayer(Player player) {
        totalPlayerCount++;

        if (gameState == GameState.RUNNING) {
            spectatorCount++;
            spectators.add(player.getUniqueId());
        } else if (gameState != GameState.ENDING && gameState != GameState.DEAD) {
            // Pre-game
            alivePlayerCount++;
            alivePlayers.add(player.getUniqueId());
        }
        addPlayerPlus(player);
    }

    public boolean isPreGame() {
        return (gameState != GameState.ENDING && gameState != GameState.RUNNING && gameState != GameState.DEAD);
    }

    public abstract void addPlayerPlus(Player player);
}
