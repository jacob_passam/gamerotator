package gamerotator.game;

public enum GameState {

    WAITING, LOBBY_CNTDN, GAME_CNTDN, RUNNING, ENDING, DEAD;
}
