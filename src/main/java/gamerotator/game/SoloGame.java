package gamerotator.game;

import gamerotator.GameManager;

public abstract class SoloGame extends Game {

    public SoloGame(GameManager gameManager, GameType gameType, int minimumPlayers, int maximumPlayers) {
        super(gameManager, gameType, minimumPlayers, maximumPlayers);
    }
}
