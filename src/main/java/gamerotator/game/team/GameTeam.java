package gamerotator.game.team;

import gamerotator.util.UtilChatColor;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class GameTeam {

    @Getter private final ChatColor color;
    @Getter private final String name;

    private final Set<UUID> teamPlayers;

    public GameTeam(ChatColor color) {
        this.color = color;
        this.name = UtilChatColor.chatColorToName(color);

        this.teamPlayers = new HashSet<>();
    }

    public void addPlayer(Player player) { teamPlayers.add(player.getUniqueId()); }
    public void removePlayer(Player player) { teamPlayers.remove(player.getUniqueId()); }

    public boolean hasPlayer(Player player) { return teamPlayers.contains(player.getUniqueId()); }

    public void clear() { teamPlayers.clear(); }
}
