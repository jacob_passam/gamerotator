package gamerotator.game;

import gamerotator.game.games.skywars.Skywars;
import lombok.Getter;
import org.bukkit.Material;

public enum GameType {

    Skywars("Skywars", "KillerTM", Material.FEATHER, gamerotator.game.games.skywars.Skywars.class);

    @Getter private final String name, description;
    @Getter private final Material icon;
    @Getter private final Class<? extends Game> game;

    GameType(String name, String description, Material icon, Class<? extends Game> game) {
        this.name = name;
        this.description = description;
        this.icon = icon;
        this.game = game;
    }
}
