package gamerotator.db;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.Getter;
import org.bson.Document;

public class MongoHandler {

    private final MongoDatabase database;

    @Getter private final MongoCollection<Document> permissions;

    public MongoHandler(MongoConnector connector) {
        this.database = connector.getClient().getDatabase("gameRotator");

        this.permissions = database.getCollection("permissions");
    }
}
