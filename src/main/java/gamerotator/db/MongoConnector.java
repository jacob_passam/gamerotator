package gamerotator.db;


import com.mongodb.MongoClient;
import com.mongodb.client.ClientSession;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.Getter;
import org.bson.Document;

public class MongoConnector {

    @Getter private final MongoClient client;

    public MongoConnector() {
        this.client = new MongoClient();
    }
}
