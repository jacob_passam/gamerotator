package gamerotator.util;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;

public class WorldUtil {

    public static World loadWorld(String name) {
        WorldCreator creator = new WorldCreator(name);
        return creator.createWorld();
     }

     public static void unloadWorld(String name, boolean save) {
         Bukkit.unloadWorld(name, save);
     }
}
