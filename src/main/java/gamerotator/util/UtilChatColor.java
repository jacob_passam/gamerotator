package gamerotator.util;

import org.bukkit.ChatColor;

public class UtilChatColor {

    public static String chatColorToName(ChatColor color) {
        switch (color) {
            case WHITE:
                return "White";
            case GOLD:
                return "Gold";
            case LIGHT_PURPLE:
                return "Light Purple";
            case AQUA:
                return "Aqua";
            case YELLOW:
                return "Yellow";
            case GREEN:
                return "Green";
            case DARK_GRAY:
                return "Dark Gray";
            case GRAY:
                return "Gray";
            case DARK_AQUA:
                return "Dark Aqua";
            case DARK_PURPLE:
                return "Dark Purple";
            case BLUE:
                return "Blue";
            case DARK_GREEN:
                return "Dark Green";
            case RED:
                return "Red";
            case BLACK:
                return "Black";
        }
        return null;

    }
}
