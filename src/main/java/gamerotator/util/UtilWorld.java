package gamerotator.util;

import gamerotator.GameRotator;
import lombok.SneakyThrows;
import net.lingala.zip4j.ZipFile;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Entity;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.InputStream;

public class UtilWorld {

    // Get world from .jar resources, possibly use a web server in the future?
    @SneakyThrows
    public static void downloadWorld(String name, JavaPlugin plugin) {
        new File("zips").mkdir();
        InputStream stream = plugin.getResource(name + ".zip");
        FileUtils.copyInputStreamToFile(stream, new File("zips/" + name + ".zip"));
    }

    @SneakyThrows
    public static World loadWorld(String name, boolean removeEntities) {
        File file = new File("zips/" + name + ".zip");

        ZipFile zipFile = new ZipFile(file);
        zipFile.extractAll(".");

        WorldCreator creator = new WorldCreator(name);
        World world = creator.createWorld();

        if (removeEntities) {
            for (Entity entity : world.getEntities()) {
                entity.remove();
            }
        }

        return world;
    }

    @SneakyThrows
    public static void cleanWorldDirectories(boolean includeWaiting) {
        File file = new File(".");
        for (File listFile : file.listFiles()) {

            if (listFile.isDirectory()) {
                if (listFile.getName().startsWith("Game-")) {
                    Bukkit.unloadWorld(listFile.getName(), false);
                    FileUtils.deleteDirectory(listFile);
                }

                if (includeWaiting && listFile.getName().equals("waiting_lobby")) {
                    Bukkit.unloadWorld(listFile.getName(), false);
                    FileUtils.deleteDirectory(listFile);
                }
            }

        }
    }
}
