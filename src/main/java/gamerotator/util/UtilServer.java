package gamerotator.util;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;

public class UtilServer {

    public static Server getServer() { return Bukkit.getServer(); }
    public static CraftServer getServerBukkit() { return ((CraftServer)Bukkit.getServer()); }

    public static void setTime(long l) {
        for (World world : getServer().getWorlds()) {
            world.setTime(l);
        }
    }

    public enum WeatherValue {
        STORM,
        THUNDERING,
        CLEAR
    }

    public static void setWeather(WeatherValue value) {
        for (World world : getServer().getWorlds()) {

            switch (value) {
                case STORM:
                    world.setStorm(true);
                    break;
                case THUNDERING:
                    world.setThundering(true);
                    break;
                case CLEAR:
                    world.setStorm(false);
                    world.setThundering(false);
                    break;
            }
        }
    }
}
